# Code challenge

## How to?
**Fork this repo then create a branch called "response"**, follow the instructions below and submit a pull-request with your results. 

## The challenge?
Create a simple weather app using Javascript/HTML/CSS that shows the current weather conditions in London, UK. Please use the API at [Open Weather Map].

## The look?
* Please base your app on the design below.
* Should be responsive
* It should be cross-browser compatible
* Please use League Gothic (available from Fontsquirrel or Adobe Edge Web Fonts)
* Please use the weather icons from [erikflowers.github.io/weather-icons/](http://erikflowers.github.io/weather-icons/)

![](weather-app.png)

Taken from this [Dribbble post](https://dribbble.com/shots/540216-Weather-app)

We do not expect you to spend a massive amount of time on this (you can if you like), so if you are unable to complete it, we would still like to see your efforts - a work-in-progress is better than nothing at all.

[Open Weather Map]: http://openweathermap.org/api